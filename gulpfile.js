const gulp = require("gulp");
const concat = require('gulp-concat');
const htmlmin = require('gulp-htmlmin');
const terser = require('gulp-terser');
const clean = require('gulp-clean');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();
const cleanCSS = require('gulp-clean-css');

// const imagemin = require('gulp-imagemin');

const cleanDist = () => {
    return gulp.src('./dist/*', { read: false, allowEmpty: true })
      .pipe(clean({ force: false }));
  }

// const dev = ()=>{
//     browserSync.init({
//         server: {
//             baseDir: "./dist"
//         }
//     });

//     gulp.watch('./src/**/*.html', gulp.series(html,()=>{browserSync.reload();}))
// }



const dev = () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
        
    });

    gulp.watch('./src/**/*', gulp.series(cleanDist, gulp.parallel(html, js, scss, img), (next) => {
        browserSync.reload();
        next();
    }))
}
const html = ()=>{
    return gulp.src('./src/**/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('./dist'))
}

const scss =()=>{
return gulp.src('./src/**/*.scss')
.pipe(sass.sync().on('error', sass.logError))
.pipe(concat('styles.min.css'))
.pipe(cleanCSS({compatibility: 'ie8'}))
.pipe(autoprefixer({
    cascade: false
}))

.pipe(gulp.dest('./dist/css'))

}

const js = ()=>{
    return gulp.src('./src/**/*.js')
    .pipe(terser())
    .pipe(gulp.dest('./dist'))
}

const img = () => {
	return gulp.src('./src/**/*.png')
		.pipe(imagemin())
		.pipe(gulp.dest('./dist'))
};


gulp.task('scss', scss)
gulp.task('html', html)
gulp.task('img', img)
gulp.task('js', js)
gulp.task('clean',cleanDist)


gulp.task("build",gulp.series(cleanDist,gulp.parallel( html,scss,js,img)))
gulp.task('dev', dev)